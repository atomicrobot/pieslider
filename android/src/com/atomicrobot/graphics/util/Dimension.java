package com.atomicrobot.graphics.util;

import android.graphics.Rect;
import android.graphics.RectF;

public class Dimension {
	public float width;
	public float height;

	public void fromRect(Rect r) {
		width = Math.abs(Math.max(r.left, r.right)
				- Math.min(r.left, r.right));
		height = Math.abs(Math.max(r.top, r.bottom)
				- Math.min(r.top, r.bottom));
	}

	public void fromRect(RectF r) {
		width = Math.abs(Math.max(r.left, r.right)
				- Math.min(r.left, r.right));
		height = Math.abs(Math.max(r.top, r.bottom)
				- Math.min(r.top, r.bottom));
	}
}