package com.atomicrobot.graphics.util;

import android.content.res.Resources;
import android.content.res.TypedArray;

public class TypedArrayUtil {

	public static int[] getColorArray(Resources r, int colorArrayId) {
		TypedArray ta = r.obtainTypedArray(colorArrayId);
		int[] colors = new int[ta.length()];
		for (int i = 0; i < ta.length(); i++) {
			colors[i] = ta.getColor(i, 0);
		}
		ta.recycle();
		return colors;
	}
	
	public static int[] getIntArray(Resources r, int intArrayId) {
		TypedArray ta = r.obtainTypedArray(intArrayId);
		int[] ints = new int[ta.length()];
		for (int i = 0; i < ta.length(); i++) {
			ints[i] = ta.getColor(i, 0);
		}
		ta.recycle();
		return ints;
	}
}
