package com.atomicrobot.graphics.util;

import android.graphics.Path;
import android.graphics.RectF;

public class ArcSpec {
	public final RectF oval;
	public final double startDegs;
	public final double sweepDegs;

	public ArcSpec(RectF oval, double startDegs, double sweepDegs) {
		this.oval = oval;
		this.startDegs = startDegs;
		this.sweepDegs = sweepDegs;
	}

	public Path toPath() {
		Path p = new Path();
		p.addArc(oval, (float) startDegs, (float) sweepDegs);
		return p;
	}
}