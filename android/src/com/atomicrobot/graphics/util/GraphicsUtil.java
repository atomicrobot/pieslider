package com.atomicrobot.graphics.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.DisplayMetrics;

public class GraphicsUtil {

	public static double toRad(double degs) {
		return Math.toRadians(degs);
	}

	public static double toDeg(double rads) {
		return Math.toDegrees(rads);
	}

	public static double sin(double rads) {
		return Math.sin(rads);
	}

	public static double cos(double rads) {
		return Math.cos(rads);
	}

	public static float min(float... vals) {
		if (vals.length < 1) {
			throw new IllegalArgumentException();
		}

		float min = vals[0];
		for (int i = 1; i < vals.length; i++) {
			min = Math.min(min, vals[i]);
		}

		return min;
	}

	public static float max(float... vals) {
		if (vals.length < 1) {
			throw new IllegalArgumentException();
		}

		float max = vals[0];
		for (int i = 1; i < vals.length; i++) {
			max = Math.max(max, vals[i]);
		}

		return max;
	}

	public static float dpToPixel(Context c, int dp) {
		DisplayMetrics dm = getDisplayMetrics(c);
		return dm.density * dp;
	}

	public static float spToPixel(Context c, int sp) {
		DisplayMetrics dm = getDisplayMetrics(c);
		return dm.scaledDensity * sp;
	}

	private static DisplayMetrics getDisplayMetrics(Context c) {
		Resources r = c.getResources();
		return r.getDisplayMetrics();
	}

	public static void calcTextBounds(Paint paint, String text, Dimension d) {
		Rect r = new Rect();
		paint.getTextBounds(text, 0, text.length(), r);
		d.fromRect(r);
	}
}
