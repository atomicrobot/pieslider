package com.atomicrobot.graphics.util;


public class AngleRange {
	public double startAngle;
	public double stopAngle;

	public void updateAngles(double startAngle, double stopAngle) {
		this.startAngle = Math.min(startAngle, stopAngle);
		this.stopAngle = Math.max(startAngle, stopAngle);
	}

	public boolean contains(double angle) {
		double startAngleUt = startAngle;
		double stopAngleUt = stopAngle;

		if (startAngleUt < 0 || stopAngleUt < 0) {
			startAngleUt += 360;
			stopAngleUt += 360;
		}

		boolean contained = ((angle < stopAngleUt) && (angle >= startAngleUt));
		if (contained) {
			return true;
		}

		contained = (((angle - 360) < stopAngleUt) && ((angle - 360) >= startAngleUt));
		if (contained) {
			return true;
		}

		contained = (((angle + 360) < stopAngleUt) && ((angle + 360) >= startAngleUt));
		if (contained) {
			return true;
		}

		return false;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ": " + startAngle + " - "
				+ stopAngle;
	}
}