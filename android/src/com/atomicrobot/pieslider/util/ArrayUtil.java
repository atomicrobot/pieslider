package com.atomicrobot.pieslider.util;

import java.lang.reflect.Array;

public class ArrayUtil {
	/**
	 * Because Array.copyOf isn't always available
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] copyOf(T[] original, Class<T> c) {
		if (original == null) {
			return null;
		}

		T[] copy = (T[]) Array.newInstance(c, original.length);
		for (int i = 0; i < original.length; i++) {
			copy[i] = original[i];
		}
		return (T[]) copy;
	}

	public static int[] copyOf(int[] original) {
		if (original == null) {
			return null;
		}

		int[] copy = new int[original.length];
		for (int i = 0; i < original.length; i++) {
			copy[i] = original[i];
		}
		return copy;
	}
}
