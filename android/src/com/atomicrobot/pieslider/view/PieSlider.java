package com.atomicrobot.pieslider.view;

import static com.atomicrobot.graphics.util.GraphicsUtil.calcTextBounds;
import static com.atomicrobot.graphics.util.GraphicsUtil.cos;
import static com.atomicrobot.graphics.util.GraphicsUtil.sin;
import static com.atomicrobot.graphics.util.GraphicsUtil.toDeg;
import static com.atomicrobot.graphics.util.GraphicsUtil.toRad;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.FontMetrics;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;

import com.atomicrobot.graphics.util.AngleRange;
import com.atomicrobot.graphics.util.ArcSpec;
import com.atomicrobot.graphics.util.Dimension;
import com.atomicrobot.graphics.util.GraphicsUtil;
import com.atomicrobot.pieslider.R;
import com.atomicrobot.pieslider.util.ArrayUtil;

public class PieSlider extends View {

	public interface OnValueChangeListener {
		public void onValueChangeStarted(PieSlider view, int index);

		public void onValueChanged(PieSlider view, int index, int value);

		public void onValueChangeCompleted(PieSlider view, int index);
	}

	static class WedgeValueCache {
		double valueRadius;

		ArcSpec valueArc;
		Path selectedStroke;

		RadialGradient valueGradient;

		WedgeValueCache(PieSlider slider, Cache c, WedgeCache wc, int index,
				int value) {
			valueRadius = c.internalArcRadius
					+ ((value + 1) * c.incrementalRadialDistance);

			cacheSelectedStrokes(slider, c, wc, value);
			cacheBackgrounds(slider, c, wc, index, value);
			cacheValueGradients(slider, c, wc, index, value);
		}

		private void cacheSelectedStrokes(PieSlider slider, Cache c,
				WedgeCache wc, int value) {
			Path valueOpenSegment = calculateWedgeSegment(slider, c,
					wc.thetaStartDegs, c.internalArcRadius, valueRadius, 90);
			Path valueCloseSegment = calculateWedgeSegment(slider, c,
					wc.thetaStopDegs, c.internalArcRadius, valueRadius, -90);
			Path valueExternalArc = calculateArc(slider, c, wc.thetaStartDegs,
					valueRadius).toPath();
			Path valueInternalArc = calculateArc(slider, c, wc.thetaStartDegs,
					c.internalArcRadius).toPath();

			selectedStroke = new Path();
			selectedStroke.addPath(valueExternalArc);
			selectedStroke.addPath(valueInternalArc);
			selectedStroke.addPath(valueCloseSegment);
			selectedStroke.addPath(valueOpenSegment);
		}

		private void cacheBackgrounds(PieSlider slider, Cache c, WedgeCache wc,
				int index, int value) {
			valueArc = calculateArc(slider, c, wc.thetaStartDegs, valueRadius);
		}

		private void cacheValueGradients(PieSlider slider, Cache c,
				WedgeCache wc, int index, int value) {
			int[] wedgeColors = slider.ringColors[index];
			valueGradient = new RadialGradient(c.centerX, c.centerY,
					(float) valueRadius, wedgeColors[0],
					wedgeColors[value + 1], TileMode.CLAMP);
		}
	}

	static class WedgeCache {
		double pieceOffset;
		double thetaStartDegs;
		double thetaStopDegs;
		double thetaMidDegs;
		double midAngle;
		double thetaOffsetDegs;
		double thetaRads;
		double thetaOffsetRads;

		float labelRotation;
		float labelYOffset;

		Path openSegment;
		Path closeSegment;
		ArcSpec externalArc;
		ArcSpec internalArc;
		WedgeValueCache[] wvc;
		Rect dirtyRegion;

		WedgeCache(PieSlider slider, Cache c, int index) {
			pieceOffset = (c.sweepDegs / 2.0) + 90.0;
			thetaStartDegs = (c.sweepDegs * index) - pieceOffset;
			thetaStopDegs = (c.sweepDegs * (index + 1)) - pieceOffset;
			thetaMidDegs = (thetaStartDegs + thetaStopDegs) / 2.0;
			midAngle = thetaStartDegs + c.sweepDegs / 2.0;
			thetaOffsetDegs = thetaStartDegs + 90.0;
			thetaRads = toRad(thetaStartDegs);
			thetaOffsetRads = toRad(thetaOffsetDegs);

			openSegment = calculateWedgeSegment(slider, c, thetaStartDegs,
					c.internalArcRadius, c.externalArcRadius, 90);
			closeSegment = calculateWedgeSegment(slider, c, thetaStopDegs,
					c.internalArcRadius, c.externalArcRadius, -90);
			externalArc = calculateArc(slider, c, thetaStartDegs,
					c.externalArcRadius);
			internalArc = calculateArc(slider, c, thetaStartDegs,
					c.internalArcRadius);

			cacheDirtyRegion(slider, c, index);
			cacheCutout(slider, c, index);
			cacheLabel(slider, c, index);
			cacheStrokes(slider, c, index);
			cacheAngleRanges(slider, c, index);

			wvc = new WedgeValueCache[c.maxValue];
			for (int j = 0; j < c.maxValue; j++) {
				wvc[j] = new WedgeValueCache(slider, c, this, index, j);
			}
		}

		private void cacheDirtyRegion(PieSlider slider, Cache c, int index) {
			double dirtyRadius = c.externalArcRadius * 1.5;
			Path start = calculateWedgeSegment(slider, c, thetaStartDegs, 0,
					dirtyRadius, 0);
			Path mid = calculateWedgeSegment(slider, c, thetaMidDegs, 0,
					dirtyRadius, 0);
			Path stop = calculateWedgeSegment(slider, c, thetaStopDegs, 0,
					dirtyRadius, 0);

			RectF startRect = new RectF();
			RectF midRect = new RectF();
			RectF stopRect = new RectF();

			start.computeBounds(startRect, true);
			mid.computeBounds(midRect, true);
			stop.computeBounds(stopRect, true);

			// The intersect API doesn't seem to be working...
			float left = GraphicsUtil.min(startRect.left, midRect.left,
					stopRect.left);
			float right = GraphicsUtil.max(startRect.right, midRect.right,
					stopRect.right);
			float top = GraphicsUtil.min(startRect.top, midRect.top,
					stopRect.top);
			float bottom = GraphicsUtil.max(startRect.bottom, midRect.bottom,
					stopRect.bottom);

			dirtyRegion = new Rect((int) left, (int) top, (int) right,
					(int) bottom);
			dirtyRegion.union(c.dirtyCenter);
		}

		private void cacheCutout(PieSlider slider, Cache c, int index) {
			double paddingXOffset = cos(thetaOffsetRads)
					* slider.internalPadding / 2.0;
			double paddingYOffset = sin(thetaOffsetRads)
					* slider.internalPadding / 2.0;

			// Extend the radius out just a little bit due to the offset
			double cutoutRadius = c.externalArcRadius + 5;
			double unpaddedStopX = c.centerX + cos(thetaRads) * cutoutRadius;
			double unpaddedStopY = c.centerY + sin(thetaRads) * cutoutRadius;

			Path wedgeCutout = new Path();
			wedgeCutout.moveTo((float) (c.centerX - paddingXOffset),
					(float) (c.centerY - paddingYOffset));
			wedgeCutout.lineTo((float) (unpaddedStopX - paddingXOffset),
					(float) (unpaddedStopY - paddingYOffset));
			wedgeCutout.lineTo((float) (unpaddedStopX + paddingXOffset),
					(float) (unpaddedStopY + paddingYOffset));
			wedgeCutout.lineTo((float) (c.centerX + paddingXOffset),
					(float) (c.centerY + paddingYOffset));
			wedgeCutout.close();

			c.cutoutPath.addPath(wedgeCutout);
		}

		private void cacheLabel(PieSlider slider, Cache c, int index) {
			FontMetrics fm = slider.labelTextPaint.getFontMetrics();
			Dimension d = new Dimension();
			d.fromRect(externalArc.oval);
			float yOffset = (d.height / 2) + fm.bottom + slider.labelPadding;
			float y = c.centerY - yOffset;
			float r = (float) (midAngle + 90);
			if (midAngle > 0 && midAngle < 180) {
				r -= 180;
				// TODO - WTF am I doing here?
				yOffset = (d.height / 2) - fm.top + slider.labelPadding;
				y = c.centerY + yOffset;
			}

			labelRotation = r;
			labelYOffset = y;
		}

		private void cacheStrokes(PieSlider slider, Cache c, int index) {
			Path dividerComposite = new Path();
			double valueDividerArcIncrement = (c.externalArcRadius - c.internalArcRadius)
					/ c.maxValue;
			for (int j = 1; j < c.maxValue; j++) {
				double valueDividerArcRadius = (valueDividerArcIncrement * j)
						+ c.internalArcRadius;
				Path valueDividerArc = calculateArc(slider, c, thetaStartDegs,
						valueDividerArcRadius).toPath();
				dividerComposite.addPath(valueDividerArc);
			}

			c.dividerStrokes.addPath(dividerComposite);

			c.strokes.addPath(externalArc.toPath());
			c.strokes.addPath(internalArc.toPath());
			c.strokes.addPath(closeSegment);
			c.strokes.addPath(openSegment);
		}

		private void cacheAngleRanges(PieSlider slider, Cache c, int index) {
			AngleRange range = new AngleRange();
			range.updateAngles(thetaStartDegs, thetaStartDegs + c.sweepDegs);
			c.pieceAngles[index] = range;
		}
	}

	static class Cache {
		int width;
		int height;
		float centerX;
		float centerY;

		int wedgeCount;
		int maxValue;

		double sweepDegs;

		double centerRadius;
		double internalArcRadius;
		double externalArcRadius;

		double valueRadialDistance;
		double incrementalRadialDistance;

		WedgeCache[] wc;
		AngleRange[] pieceAngles;

		Path cutoutPath;
		Path dividerStrokes;
		Path strokes;
		Rect dirtyCenter;

		void cacheCalculations(PieSlider slider, int wedges, int maxValue,
				int width, int height) {
			String sampleMaxCenterText = "";
			for (int i = 0; i < slider.maxCenterTextLength; i++) {
				sampleMaxCenterText += "X";
			}
			calcTextBounds(slider.centerTextPaint, sampleMaxCenterText,
					slider.maxCenterTextBounds);

			// TODO - Why "Apple"?
			calcTextBounds(slider.labelTextPaint, "Apple",
					slider.labelTextBounds);

			// TODO: Why XXX?
			float maxCenterTextWidth = slider.centerTextPaint
					.measureText("XXX");
			centerRadius = (maxCenterTextWidth / 2) + slider.centerTextPadding;

			this.width = width;
			this.height = height;
			centerX = width / 2.0f;
			centerY = height / 2.0f;

			float length = Math.min(width, height);
			externalArcRadius = (length / 2.0) - slider.padding
					- slider.labelTextBounds.height;
			if (externalArcRadius <= 0) {
				throw new IllegalStateException(
						"The outside arc has a negative length because length: "
								+ length + " padding: " + slider.padding
								+ " label:height: "
								+ slider.labelTextBounds.height);
			}

			wedgeCount = wedges;
			this.maxValue = maxValue;
			sweepDegs = (360.0 / wedges);

			cutoutPath = new Path();
			strokes = new Path();
			dividerStrokes = new Path();
			pieceAngles = new AngleRange[wedgeCount];
			wc = new WedgeCache[wedges]; // TODO - Kill wedges or wedgeCount

			internalArcRadius = centerRadius + slider.internalPadding;
			valueRadialDistance = (externalArcRadius - internalArcRadius);
			incrementalRadialDistance = valueRadialDistance / maxValue;

			int centerLeft = (int) (centerX - centerRadius);
			int centerTop = (int) (centerY - centerRadius);
			int centerRight = (int) (centerX + centerRadius);
			int centerBottom = (int) (centerY + centerRadius);

			dirtyCenter = new Rect(centerLeft, centerTop, centerRight,
					centerBottom);

			slider.fillPaint.setShader(new RadialGradient(centerX, centerY,
					(float) externalArcRadius, Color.parseColor("#20000000"),
					Color.parseColor("#10000000"), TileMode.CLAMP));

			for (int i = 0; i < wedgeCount; i++) {
				wc[i] = new WedgeCache(slider, this, i);
			}
		}
	}

	private static ArcSpec calculateArc(PieSlider slider, Cache c,
			double thetaStartDegs, double radius) {
		RectF oval = new RectF(//
				(float) (c.centerX - radius), //
				(float) (c.centerY - radius), //
				(float) (c.centerX + radius), //
				(float) (c.centerY + radius));
		double paddingOffsetDegrees = toDeg(slider.internalPadding / radius);
		double arcStartDegs = thetaStartDegs + (paddingOffsetDegrees / 2.0);
		double arcSweep = c.sweepDegs - paddingOffsetDegrees;

		return new ArcSpec(oval, arcStartDegs, arcSweep);
	}

	private static Path calculateWedgeSegment(PieSlider slider, Cache c,
			double thetaDegs, double startRadius, double stopRadius,
			int thetaPerpendicularOffsetDegs) {
		double thetaRads = toRad(thetaDegs);
		double thetaPerpendicular = thetaDegs + thetaPerpendicularOffsetDegs;
		double thetaPerpendicularRads = toRad(thetaPerpendicular);

		double startXOffset = cos(thetaRads) * startRadius;
		double startYOffset = sin(thetaRads) * startRadius;
		double stopXOffset = cos(thetaRads) * stopRadius;
		double stopYOffset = sin(thetaRads) * stopRadius;

		double internalPaddingOffset = slider.internalPadding / 2.0;
		double xPaddingOffset = cos(thetaPerpendicularRads)
				* internalPaddingOffset;
		double yPaddingOffset = sin(thetaPerpendicularRads)
				* internalPaddingOffset;

		float startX = (float) (c.centerX + startXOffset + xPaddingOffset);
		float startY = (float) (c.centerY + startYOffset + yPaddingOffset);
		float stopX = (float) (c.centerX + stopXOffset + xPaddingOffset);
		float stopY = (float) (c.centerY + stopYOffset + yPaddingOffset);

		Path p = new Path();
		p.moveTo(startX, startY);
		p.lineTo(stopX, stopY);
		return p;
	}

	private Paint fillPaint;
	private Paint wedgeFillPaint;
	private Paint cutoutPaint;

	private Paint strokePaint;
	private Paint dividerStrokePaint;
	private Paint selectedStrokePaint;

	private Paint centerBackgroundPaint;
	private Paint centerTextPaint;
	private Paint labelTextPaint;

	private Dimension maxCenterTextBounds;
	private Dimension labelTextBounds;

	private float padding;
	private float internalPadding;
	private float centerTextPadding;
	private int maxCenterTextLength;
	private float labelPadding;
	private int[][] ringColors;
	private int vibrateMs;

	private String centerText;
	private String[] labels;
	private int[] values;

	private boolean readOnly = false;

	private int currentlyTouchingPiePiece = -1;

	private OnValueChangeListener valueChangeListener;
	private Vibrator vibrator;

	protected Cache c = new Cache();
	private Bitmap backgroundCache;
	private Bitmap valueCache;
	private Bitmap strokesCache;

	/*
	 * Initialization methods
	 */

	public PieSlider(Context context) {
		super(context);
		init();
	}

	public PieSlider(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();

		TypedArray ta = context.obtainStyledAttributes(attrs,
				R.styleable.PieSlider);
		styleLabelText(context, ta);
		styleCenterText(context, ta);

		int padding = ta
				.getDimensionPixelSize(R.styleable.PieSlider_padding, 0);
		setPadding(padding);

		int internalPadding = ta.getDimensionPixelSize(
				R.styleable.PieSlider_internalPadding, 0);
		setInternalPadding(internalPadding);

		int centerLabelPadding = ta.getDimensionPixelSize(
				R.styleable.PieSlider_centerLabelPadding, 0);
		setCenterLabelPadding(centerLabelPadding);

		int outerLabelPadding = ta.getDimensionPixelSize(
				R.styleable.PieSlider_outerLabelPadding, 0);
		setOuterLabelPadding(outerLabelPadding);

		int strokeWidth = ta.getInt(R.styleable.PieSlider_strokeWidth, 0);
		setStrokeWidth(strokeWidth);

		int strokeColor = ta.getColor(R.styleable.PieSlider_strokeColor, 0);
		setStrokeColor(strokeColor);

		int dividerStrokeWidth = ta.getInt(
				R.styleable.PieSlider_dividerStrokeWidth, 0);
		setDividerStrokeWidth(dividerStrokeWidth);

		int dividerStrokeColor = ta.getColor(
				R.styleable.PieSlider_dividerStrokeColor, 0);
		setDividerStrokeColor(dividerStrokeColor);

		int selectedStrokeWidth = ta.getInt(
				R.styleable.PieSlider_selectedStrokeWidth, 0);
		setSelectedStrokeWidth(selectedStrokeWidth);

		int selectedStrokeColor = ta.getColor(
				R.styleable.PieSlider_selectedStrokeColor, 0);
		setSelectedStrokeColor(selectedStrokeColor);

		int maxCenterLength = ta.getInt(R.styleable.PieSlider_maxCenterLength,
				0);
		if (maxCenterLength == 0) {
			throw new IllegalStateException(
					"The attribute maxCenterLength must be defined and greater than 0");
		}
		setMaxCenterTextLength(maxCenterLength);

		int vibrateMs = ta.getInt(R.styleable.PieSlider_vibrateMs, 0);
		setVibrateMs(vibrateMs);

		ta.recycle();
	}

	private void init() {
		maxCenterTextBounds = new Dimension();
		labelTextBounds = new Dimension();

		fillPaint = buildPaint();
		fillPaint.setStyle(Paint.Style.FILL);

		wedgeFillPaint = buildPaint();
		wedgeFillPaint.setStyle(Paint.Style.FILL);

		cutoutPaint = buildPaint();
		cutoutPaint.setStyle(Paint.Style.FILL);
		cutoutPaint.setColor(Color.TRANSPARENT);
		cutoutPaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));

		strokePaint = buildPaint();
		strokePaint.setStyle(Paint.Style.STROKE);
		strokePaint.setStrokeCap(Cap.ROUND);

		dividerStrokePaint = buildPaint();
		dividerStrokePaint.setStyle(Paint.Style.STROKE);
		dividerStrokePaint.setStrokeCap(Cap.ROUND);

		selectedStrokePaint = buildPaint();
		selectedStrokePaint.setStyle(Paint.Style.STROKE);
		selectedStrokePaint.setStrokeCap(Cap.ROUND);

		centerBackgroundPaint = buildPaint();
		centerBackgroundPaint.setStyle(Paint.Style.FILL);

		centerTextPaint = buildPaint();
		centerTextPaint.setTextAlign(Align.CENTER);

		labelTextPaint = buildPaint();
		labelTextPaint.setTextAlign(Align.CENTER);

		vibrator = (Vibrator) getContext().getSystemService(
				Context.VIBRATOR_SERVICE);

		// Allows us to catch motion events, per http://www.qc4blog.com/?p=1181
		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			}
		});
	}

	private Paint buildPaint() {
		return new Paint(Paint.ANTI_ALIAS_FLAG);
	}

	private void styleLabelText(Context context, TypedArray attrs) {
		int labelStyleId = attrs.getResourceId(
				R.styleable.PieSlider_outerLabelTextStyle,
				android.R.style.TextAppearance_Medium);
		TypedArray ta = context.obtainStyledAttributes(labelStyleId, new int[] {
				android.R.attr.textSize, // 0
				android.R.attr.textColor // 1
				});
		int textSize = ta.getDimensionPixelSize(0, 0);
		int textColor = ta.getColor(1, 0);

		setLabelTextSize(textSize);
		setLabelTextColor(textColor);

		ta.recycle();
	}

	private void styleCenterText(Context context, TypedArray attrs) {
		int labelStyleId = attrs.getResourceId(
				R.styleable.PieSlider_centerLabelStyle,
				android.R.style.TextAppearance_Large);
		TypedArray ta = context.obtainStyledAttributes(labelStyleId, new int[] {
				android.R.attr.textSize, // 0
				android.R.attr.textColor // 1
				});
		int textSize = ta.getDimensionPixelSize(0, 0);
		int textColor = ta.getColor(1, 0);

		setCenterTextSize(textSize);
		setCenterTextColor(textColor);

		ta.recycle();
	}

	/*
	 * Touch methods
	 */

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		synchronized (this) {
			handleTouchEvent(event);
			return true;
		}
	}

	private void handleTouchEvent(MotionEvent event) {
		boolean upEvent = false;
		if (event.getAction() == MotionEvent.ACTION_UP) {
			if (valueChangeListener != null) {
				if (currentlyTouchingPiePiece != -1) {
					valueChangeListener.onValueChangeCompleted(this,
							currentlyTouchingPiePiece);
				}
			}

			int lastWedge = currentlyTouchingPiePiece;
			upEvent = true;
			currentlyTouchingPiePiece = -1;
			invalidateForWedge(lastWedge);
		}

		float eventX = event.getX();
		float eventY = event.getY();
		float xFromCenter = eventX - c.centerX;
		float yFromCenter = eventY - c.centerY;
		float distanceFromCenter = FloatMath.sqrt((xFromCenter * xFromCenter)
				+ (yFromCenter * yFromCenter));
		if (distanceFromCenter > c.externalArcRadius) {
			return;
		}

		double distanceToValue = (c.centerRadius + internalPadding);
		if (distanceFromCenter < distanceToValue) {
			return;
		}

		double radialValueDistance = c.externalArcRadius - distanceToValue;
		double incrementalRadialValueDistance = radialValueDistance
				/ (ringColors[0].length - 1);
		double eventValueDistance = distanceFromCenter - distanceToValue;
		int value = 1;
		for (int i = 1; i < ringColors[0].length; i++) {
			if (eventValueDistance < (i * incrementalRadialValueDistance)) {
				value = i;
				break;
			}
		}

		double angleFromOriginRads = Math.atan2(yFromCenter, xFromCenter);
		double angleFromOrigin = Math.toDegrees(angleFromOriginRads);

		for (int i = 0; i < c.pieceAngles.length; i++) {
			AngleRange ar = c.pieceAngles[i];
			if (ar.contains(angleFromOrigin)) {
				if (currentlyTouchingPiePiece != i && upEvent) {
					break;
				} else if (currentlyTouchingPiePiece == -1) {
					currentlyTouchingPiePiece = i;
				} else {
					if (i != currentlyTouchingPiePiece) {
						break;
					}
				}

				if (values[i] != value) {
					values[i] = value;

					if (valueChangeListener != null) {
						valueChangeListener.onValueChanged(this, i, value);
					}

					vibrate();
					invalidateForWedge(i);
				} else {
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						if (valueChangeListener != null) {
							valueChangeListener.onValueChangeStarted(this, i);
						}

						currentlyTouchingPiePiece = i;
						vibrate();
						invalidateForWedge(i);
					}
				}

				break;
			}
		}
	}

	private void vibrate() {
		if (vibrateMs > 0) {
			vibrator.vibrate(vibrateMs);
		}
	}

	/*
	 * Drawing methods
	 */

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		synchronized (this) {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);

			int width = MeasureSpec.getSize(widthMeasureSpec);
			int height = MeasureSpec.getSize(heightMeasureSpec);
			int smallestDimension = Math.min(width, height);
			setMeasuredDimension(smallestDimension, smallestDimension);

			if (c.width != smallestDimension || c.height != smallestDimension
					|| c.wedgeCount != labels.length) {
				backgroundCache = null;
				valueCache = null;
				strokesCache = null;

				int maxValue = ringColors[0].length - 1;
				int wedges = labels.length;
				c.cacheCalculations(this, wedges, maxValue, smallestDimension,
						smallestDimension);
			}
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		synchronized (this) {
			super.onDraw(canvas);

			drawBackground(canvas);
			updateValueBackgrounds(canvas);

			drawStrokes(canvas);
			drawSelectionStrokes(canvas);

			updateCenterText(canvas);
			updateLabels(canvas);
		}
	}

	private Bitmap createBitmap(Canvas canvas) {
		return Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(),
				Config.ARGB_8888);
	}

	private void drawCutouts(Canvas canvas, Mode mode) {
		canvas.drawCircle(c.centerX, c.centerY, (float) c.internalArcRadius,
				cutoutPaint);

		cutoutPaint.setXfermode(new PorterDuffXfermode(mode));
		canvas.drawPath(c.cutoutPath, cutoutPaint);
	}

	private void drawBackground(Canvas canvas) {
		if (backgroundCache == null) {
			backgroundCache = createBitmap(canvas);
			Canvas backgroundCanvas = new Canvas(backgroundCache);
			backgroundCanvas.drawColor(Color.TRANSPARENT);
			backgroundCanvas.drawCircle(c.centerX, c.centerY,
					(float) c.externalArcRadius, fillPaint);

			drawCutouts(backgroundCanvas, Mode.CLEAR);
		}

		canvas.drawBitmap(backgroundCache, 0, 0, null);
	}

	private void updateValueBackgrounds(Canvas canvas) {
		// Write to a background bitmap so we can also draw cutouts
		if (valueCache == null) {
			valueCache = createBitmap(canvas);
		}

		Canvas valueCanvas = new Canvas(valueCache);
		valueCanvas.drawColor(Color.TRANSPARENT, Mode.CLEAR);

		for (int i = 0; i < c.wedgeCount; i++) {
			int value = values[i];
			int valueIndex = value - 1;
			WedgeCache wc = c.wc[i];
			WedgeValueCache wvc = wc.wvc[valueIndex];

			wedgeFillPaint.setShader(wvc.valueGradient);
			valueCanvas.drawArc(wvc.valueArc.oval,
					(float) wvc.valueArc.startDegs,
					(float) wvc.valueArc.sweepDegs, true, wedgeFillPaint);

			drawCutouts(valueCanvas, Mode.CLEAR);
		}

		canvas.drawBitmap(valueCache, 0, 0, null);
	}

	private void drawStrokes(Canvas canvas) {
		if (strokesCache == null) {
			strokesCache = createBitmap(canvas);
			Canvas strokesCanvas = new Canvas(strokesCache);

			strokesCanvas.drawPath(c.dividerStrokes, dividerStrokePaint);

			strokesCanvas.drawPath(c.strokes, strokePaint);
		}

		canvas.drawBitmap(strokesCache, 0, 0, null);
	}

	private void drawSelectionStrokes(Canvas canvas) {
		if (currentlyTouchingPiePiece >= 0) {

			int wedgeIndex = currentlyTouchingPiePiece;
			WedgeCache wc = c.wc[wedgeIndex];
			int value = values[wedgeIndex];
			WedgeValueCache wvc = wc.wvc[value - 1];

			canvas.drawPath(wvc.selectedStroke, selectedStrokePaint);
		}
	}

	private void updateCenterText(Canvas canvas) {
		if (!TextUtils.isEmpty(centerText)) {
			Dimension centerBounds = new Dimension();
			calcTextBounds(centerTextPaint, centerText, centerBounds);

			canvas.drawText(centerText, c.centerX, c.centerY
					+ centerBounds.height / 2, centerTextPaint);
		}
	}

	private void updateLabels(Canvas canvas) {
		for (int i = 0; i < c.wedgeCount; i++) {
			WedgeCache wc = c.wc[i];

			canvas.save();
			canvas.rotate(wc.labelRotation, c.centerX, c.centerY);
			CharSequence label = labels[i];
			canvas.drawText(label.toString(), c.centerX, wc.labelYOffset,
					labelTextPaint);
			canvas.restore();
		}
	}

	private void invalidateForWedge(int wedge) {
		if (wedge < 0 || wedge > c.wc.length) {
			return;
		}

		WedgeCache wc = c.wc[wedge];
		invalidate(wc.dirtyRegion);
	}

	/*
	 * View properties
	 */

	public void setCenterTextSize(int centerTextSize) {
		synchronized (this) {
			centerTextPaint.setTextSize(centerTextSize);
			invalidate();
		}
	}

	public void setLabelTextSize(int labelTextSize) {
		synchronized (this) {
			labelTextPaint.setTextSize(labelTextSize);
			invalidate();
		}
	}

	public void setCenterTextColor(int centerTextColor) {
		synchronized (this) {
			centerTextPaint.setColor(centerTextColor);
			invalidate();
		}
	}

	public void setLabelTextColor(int labelTextColor) {
		synchronized (this) {
			labelTextPaint.setColor(labelTextColor);
			invalidate();
		}
	}

	public void setPadding(int padding) {
		synchronized (this) {
			this.padding = padding;
			invalidate();
		}
	}

	public void setInternalPadding(int internalPadding) {
		synchronized (this) {
			this.internalPadding = internalPadding;
			invalidate();
		}
	}

	public void setCenterLabelPadding(int centerTextPadding) {
		synchronized (this) {
			this.centerTextPadding = centerTextPadding;
			invalidate();
		}
	}

	public void setOuterLabelPadding(int labelPadding) {
		synchronized (this) {
			this.labelPadding = labelPadding;
			invalidate();
		}
	}

	public void setStrokeWidth(int strokeWidth) {
		synchronized (this) {
			strokePaint.setStrokeWidth(strokeWidth);
			invalidate();
		}
	}

	public void setStrokeColor(int strokeColor) {
		synchronized (this) {
			strokePaint.setColor(strokeColor);
			invalidate();
		}
	}

	public void setDividerStrokeWidth(int secondaryStrokeWidth) {
		synchronized (this) {
			dividerStrokePaint.setStrokeWidth(secondaryStrokeWidth);
			invalidate();
		}
	}

	public void setDividerStrokeColor(int secondaryStrokeColor) {
		synchronized (this) {
			dividerStrokePaint.setColor(secondaryStrokeColor);
			invalidate();
		}
	}

	public void setSelectedStrokeWidth(int selectedStrokeWidth) {
		synchronized (this) {
			selectedStrokePaint.setStrokeWidth(selectedStrokeWidth);
			invalidate();
		}
	}

	public void setSelectedStrokeColor(int selectedStrokeColor) {
		synchronized (this) {
			selectedStrokePaint.setColor(selectedStrokeColor);
			invalidate();
		}
	}

	public void updateData(String[] labels, int[][] ringColors, int[] values) {
		synchronized (this) {
			setRingColors(ringColors);
			updateData(labels, values);
		}
	}

	public void updateData(String[] labels, int[] values) {
		synchronized (this) {
			if (labels == null) {
				throw new IllegalArgumentException(
						"Labels to display must not be null");
			} else if (values == null) {
				throw new IllegalArgumentException(
						"Values to display must not be null");
			} else if (labels.length != values.length) {
				throw new IllegalArgumentException(
						"Different number of labels and values");
			} else if (labels.length < 2) {
				throw new IllegalArgumentException(
						"Must define at least two labels");
			} else if (values.length < 2) {
				throw new IllegalArgumentException(
						"Must define at least two values");
			}

			assertValuesLessThanMax(values);

			this.labels = ArrayUtil.copyOf(labels, String.class);
			this.values = ArrayUtil.copyOf(values);
			requestLayout();
			invalidate();
		}
	}

	private void assertValuesLessThanMax(int[] values) {
		for (int i = 0; i < values.length; i++) {
			if (values[i] > ringColors[i].length) {
				throw new IllegalArgumentException("A provided value of "
						+ values[i] + " exceeds the current max value of "
						+ ringColors.length);
			}
		}
	}

	public String[] getLabels() {
		return ArrayUtil.copyOf(labels, String.class);
	}

	public String getLabel(int index) {
		return labels[index];
	}

	/**
	 * Significantly better performance than updating all labels
	 */
	public void setLabel(int index, String label) {
		synchronized (this) {
			labels[index] = label;
			invalidateForWedge(index);
		}
	}

	public void setRingColors(int[][] ringColors) {
		synchronized (this) {
			this.ringColors = ringColors;
			updateColorCache();
			invalidate();
		}
	}

	private void updateColorCache() {
		if (c != null && c.wc != null) {
			for (int i = 0; i < c.wc.length; i++) {
				WedgeCache wc = c.wc[i];
				for (int j = 0; j < wc.wvc.length; j++) {
					WedgeValueCache wvc = wc.wvc[j];
					wvc.cacheValueGradients(this, c, wc, i, j);
				}
			}
		}
	}

	public void setMaxCenterTextLength(int maxCenterTextLength) {
		synchronized (this) {
			this.maxCenterTextLength = maxCenterTextLength;
			invalidate();
		}
	}

	public int[] getValues() {
		return ArrayUtil.copyOf(values);
	}

	public void setValues(int[] values) {
		synchronized (this) {
			if (values == null) {
				throw new IllegalArgumentException(
						"Values to display must not be null");
			} else if (labels.length != values.length) {
				throw new IllegalArgumentException(
						"Different number of labels and values");
			} else if (values.length < 2) {
				throw new IllegalArgumentException(
						"Must define at least two values");
			}

			assertValuesLessThanMax(values);

			this.values = ArrayUtil.copyOf(values);
			invalidate();
		}
	}

	public void setCenterText(String centerText) {
		synchronized (this) {
			this.centerText = centerText;
			invalidate(c.dirtyCenter);
		}
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		synchronized (this) {
			this.readOnly = readOnly;
		}
	}

	public void setVibrateMs(int vibrateMs) {
		synchronized (this) {
			this.vibrateMs = vibrateMs;
		}
	}

	public void setOnValueChangeListener(
			OnValueChangeListener valueChangeListener) {
		synchronized (this) {
			this.valueChangeListener = valueChangeListener;
		}
	}
}